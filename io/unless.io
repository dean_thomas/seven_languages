unless := method(
    (call sender doMessage(call message argAt(0))) ifFalse(
        call sender doMessage(call message argAt(1))) ifTrue(
            (call sender doMessage(call message argAt(2))))
)

unless(1 == 2, write("One is not two\n"), write("one is two\n"))
unless(3 == 3, write("Three is not three\n"), write("three is three\n"))
