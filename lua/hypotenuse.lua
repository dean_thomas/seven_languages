function hypotenuse(a, b)
    local a2 = a * a
    local b2 = b * b
    return math.sqrt(a2 + b2)
end


print("a = 1; b = 2; h = " .. hypotenuse(1, 2))
print("a = 3; b = 4; h = " .. hypotenuse(3, 4))
print("a = 7; b = 8; h = " .. hypotenuse(7, 8))