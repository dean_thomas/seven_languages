cmake_minimum_required (VERSION 2.8)
project(play)
add_executable(play play.cpp)

#   https://github.com/thestk/rtmidi/issues/97
find_library(RTMIDI rtmidi REQUIRED)
find_library(LUA lua5.2 REQUIRED)

include_directories(/usr/include/lua5.2 /usr/include/rtmidi)

target_link_libraries(play ${LUA} ${RTMIDI})