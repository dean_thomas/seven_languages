Build container using:

```docker-compose build lua_midi```

Run container using:

```docker-compose run lua_midi <name-of-script.lua>```

Example:

```docker-compose run lua_midi canon.lua```