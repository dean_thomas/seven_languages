extern "C"
{
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

//  See: http://tedfelix.com/linux/linux-midi.html for setting up Midi audio on Linux
#include "RtMidi.h"
#include <vector>

static RtMidiOut midi;

int midi_send(lua_State *L)
{
    double status = lua_tonumber(L, -3);
    double data1 = lua_tonumber(L, -2);
    double data2 = lua_tonumber(L, -1);

    std::vector<unsigned char> message(3);
    message[0] = static_cast<unsigned char>(status);
    message[1] = static_cast<unsigned char>(data1);
    message[2] = static_cast<unsigned char>(data2);

    midi.sendMessage(&message);
    return 0;
}

int main(int argc, const char *argv[])
{
    if (argc < 1)
    {
        return -1;
    }

    unsigned int ports = midi.getPortCount();
    if (ports < 1)
    {
        return -1;
    }

    for (unsigned int i = 0; i < ports; i++)
    {
        try
        {
            auto portName = midi.getPortName(i);
            std::cout << "  Output Port #" << i + 1 << ": " << portName << '\n';
        }
        catch (RtMidiError &error)
        {
            error.printMessage();
            return -1;
        }
    }
    std::cout << '\n';

    //  note: on linux FluidSynth appears as the second port
    midi.openPort(1);

    lua_State *L = luaL_newstate();
    luaL_openlibs(L);

    lua_pushcfunction(L, midi_send);
    lua_setglobal(L, "midi_send");

    luaL_dostring(L, "song = require 'notation'");
    auto r = luaL_dofile(L, argv[1]);
    if (r != 0)
    {
        std::cout << "An error occured in the Lua script: " << r << std::endl;
        std::cout << "Error: " << lua_tostring(L, -1) << std::endl;
    }
    luaL_dostring(L, "song.go()");

    lua_close(L);
    return 0;
}